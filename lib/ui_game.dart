import 'package:flutter/material.dart';

/*
  La UIGame è composta da 2 parti: una sopra che rappresenta le barre di vita, relazione con gli alieni
  completamento dell'obiettivo principale, ecc...
  La parte di sotto rappresenta un testo che spiega la situazione e la card corrente che bisogna
  trascinarla o a sinistra, oppure a destra per eseguire un azione.
*/

class UIGame extends StatefulWidget
{
  @override
  State createState() => new UIGameState();
}

class UIGameState extends State<UIGame>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      /*appBar: new AppBar
      (
        title: new Text("Get me home"),
        elevation: 0.0,
      ),*/
      body: new Column
      (
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new ViewStatusBars(),
          new Expanded
          (
            child: new ViewCardSwipe(),
          )
        ],
      ),
    );
  }
}

class ViewStatusBars extends StatefulWidget
{
  @override
  State createState() => new ViewStatusBarsState();
}

class ViewStatusBarsState extends State<ViewStatusBars>
{
  @override
  Widget build(BuildContext context)
  {
    return new Material
    (
      color: Colors.grey[900],
      child: new Container
      (
        margin: new EdgeInsets.only(top: 52.0, bottom: 32.0),
        child: new Row
        (
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Icon(Icons.ac_unit, color: Colors.tealAccent, size: 54.0),
            new Icon(Icons.format_align_justify, color: Colors.tealAccent, size: 54.0),
            new Icon(Icons.featured_video, color: Colors.tealAccent, size: 54.0),
            new Icon(Icons.fastfood, color: Colors.tealAccent, size: 54.0)
          ],
        ),
      )
    );
  }
}

class ViewCardSwipe extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new Container
    (
      color: Colors.orange,
      padding: new EdgeInsets.all(8.0),
      child: new Column
      (
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new Container
          (
            margin: new EdgeInsets.all(8.0),
            child: new Text
            (
              "The alien seems to point his hand to a hole in the ground",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.title.copyWith(color: Colors.black),
            ),
          ),
          new Center
          (
            child: new SizedBox.fromSize
            (
              size: new Size(312.0, 354.0),
              child: new Card
              (
                elevation: 8.0,
                child: new Icon(Icons.filter_hdr, color: Colors.brown),
              ),
            ),
          ),
        ],
      )
    );
  }
}